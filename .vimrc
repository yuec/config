" All system-wide defaults are set in $VIMRUNTIME/debian.vim (usually just
" /usr/share/vim/vimcurrent/debian.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vim/vimrc), since debian.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing debian.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
runtime! debian.vim

" Uncomment the next line to make Vim more Vi-compatible
" NOTE: debian.vim sets 'nocompatible'.  Setting 'compatible' changes numerous
" options, so any other options should be set AFTER setting 'compatible'.
"set compatible

" Vim5 and later versions support syntax highlighting. Uncommenting the
" following enables syntax highlighting by default.
if has("syntax")
  syntax on
endif

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
"set background=dark

" Uncomment the following to have Vim jump to the last position when
" reopening a file
"if has("autocmd")
"  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
"endif

" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
"if has("autocmd")
"  filetype plugin indent on
"endif

" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
"set showcmd		" Show (partial) command in status line.
"set showmatch		" Show matching brackets.
"set ignorecase		" Do case insensitive matching
"set smartcase		" Do smart case matching
"set incsearch		" Incremental search
"set autowrite		" Automatically save before commands like :next and :make
"set hidden             " Hide buffers when they are abandoned
"set mouse=a		" Enable mouse usage (all modes)
syntax on

set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent
set cindent
set cinoptions={0,1s,t0,n-2,p2s,(03s,=.5s,>1s,=1s,:1s)}
set nu
set ruler
set textwidth=80
set wrap
set tags=tags;/

" cscope
cs add cscope.out

let treeExplWinSize=30
let treeExplVertical=20
let treeExplDirSort=1
let treeExplNoList=1
map <F2> :VSTreeExplore<cr>:set nonu<cr>
map <F3> :DoxLic<cr>
map <F4> :Dox<cr>
let g:DoxygenToolkit_commentType = "C"

let Tlist_Ctags_Cmd = "/usr/bin/ctags"
let Tlist_WinWidth = 50
map <F5> :TlistToggle<cr>

"map <c-\> :tab split<cr>:exec("tag ".expand("<cword>"))<cr>
"map <a-]> :vsp <cr>:exec("tag ".expand("<cword>"))<cr>

"let g:DoxygenToolkit_briefTag_pre="@Synopsis  "
"let g:DoxygenToolkit_paramTag_pre="@Param "
"let g:DoxygenToolkit_returnTag="@Returns   "
"let g:DoxygenToolkit_blockHeader="--------------------------------------------------------------------------"
"let g:DoxygenToolkit_blockFooter="----------------------------------------------------------------------------"
"let g:DoxygenToolkit_authorName="Yue Cheng"
"let g:DoxygenToolkit_licenseTag="My own license"

set t_Co=256
colorscheme zenburn

" For solarized setting
"option name               default     optional
"------------------------------------------------
"g:solarized_termcolors=   16      |   256
"g:solarized_termtrans =   0       |   1
"g:solarized_degrade   =   0       |   1
"g:solarized_bold      =   1       |   0
"g:solarized_underline =   1       |   0
"g:solarized_italic    =   1       |   0
"g:solarized_contrast  =   "normal"|   "high" or "low"
"g:solarized_visibility=   "normal"|   "high" or "low"
"------------------------------------------------
"let g:solarized_termcolors=16
"let g:solarized_termtrans=0
"let g:solarized_degrade=0   
"let g:solarized_bold=1       
"let g:solarized_underline=1       
"let g:solarized_italic=1       
"let g:solarized_contrast="normal"
"let g:solarized_visibility="normal"
"
"syntax enable
"set background=dark
"colorscheme solarized

" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif

